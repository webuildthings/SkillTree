为了保密或者提高运行速度, 我们可以使用c/c++实现功能然后使用NDK构建代码生成库提供给Android使用     

首先来看看[NDK](https://developer.android.google.cn/ndk/downloads)到底是什么？    

```
Android NDK
Android NDK 是一个工具集    
可让您使用 C 和 C++ 等语言以原生代码实现应用的各个部分    
对于特定类型的应用，这可以帮助您重复使用以这些语言编写的代码库    
```    

NDK在实际使用时是使用`ndk-build`脚本来进行编译的    
`ndk-build`脚本需要两个配置文件`Android.mk` 和 `Application.mk`    
将源码和配置文件放入一个名称为`jni`的文件夹内,执行`ndk-build`命令即可         


# 例程
## 目录结构
```
demo/
└── jni
    ├── Android.mk            /* ndk-build 配置文件     */
    ├── Application.mk        /* ndk-build 配置文件     */
    ├── com_demo_sumJNI.c     /* 供java调用的接口文件   */
    ├── com_demo_sumJNI.h
    ├── sum.c                 /* 功能函数文件           */
    └── sum.h
```

## 执行编译
1. `cd demo`
2. `ndk-build`
执行成功后会生成动态库 `libsum.so`  (动态库名字可自己定义)

## 说明    
将功能模块打包成库提供给Android使用，需要使用ndk-build脚本来构建项目    
- ndk-build需要 `Android.mk` 和 `Application.mk` 配置文件    
- c/c++库文件需要提供jni接口给java层调用


### sum.c
功能函数    
```
int sum(int a, int b){
    return (a+b);
}
```


### com_demo_sumJNI.c 
此文件为jni层,java层必须通过jni才能调用c/c++库    
jni接口是通过 javac 生成(jdk10以前的老版本用javah命令)      
```
#include <jni.h>
#include "sum.h"
JNIEXPORT jint JNICALL Java_com_demo_sumJNI_sum
  (JNIEnv *env, jobject thiz, jint a, jint b)
  {
      return sum(a,b);
  }
```



### Android.mk
```
include $(CLEAR_VARS)		                # 清除LOCAL_PATH以外的变量
LOCAL_MODULE := sum                         # 动态库名称
LOCAL_SRC_FILES := sum.c                    # 源文件
LOCAL_SRC_FILES += com_demo_sumJNI.c
include $(BUILD_SHARED_LIBRARY)             # 生成动态库
```


### Application.mk
```
APP_ABI := armeabi-v7a arm64-v8a            # 待生成的目标平台
APP_PLATFORM := android-16                  # Android API 级别， 最低支持的级别
```







参考    
NDK下载 [https://developer.android.google.cn/ndk/downloads](https://developer.android.google.cn/ndk/downloads)    
NDK官方手册 [https://developer.android.google.cn/ndk/guides](https://developer.android.google.cn/ndk/guides)     
NDK官方Demo [https://github.com/android/ndk-samples/tree/master](https://github.com/android/ndk-samples/tree/master)      
JNI官方说明[https://docs.oracle.com/en/java/javase/14/docs/specs/jni/](https://docs.oracle.com/en/java/javase/14/docs/specs/jni/)





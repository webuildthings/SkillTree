
>
> [官网 https://www.docker.com/](https://www.docker.com/)  
> [官方文档](https://docs.docker.com/)  
>  
> [Docker](https://www.docker.com/)是 PaaS 提供商dotCloud开源的一个基于 LXC 的高级容器引擎， [源代码](https://github.com/docker/docker)托管在 Github 上, 基于go语言并遵从Apache2.0协议开源.  
>  


## 基本概念  
1. `镜像`: 镜像是一个只读的模板.用来创建 Docker 容器.  
2. `容器`: 容器是从镜像创建的运行实例.用于运行应用.每个容器都是相互隔离的、保证安全的平台.镜像是只读的，容器在启动的时候创建一层可写层作为最上层.  
3. `仓库`: 仓库是集中存放镜像文件的场所.官方仓库[Docker Hub](https://hub.docker.com/)，存放了数量庞大的镜像供用户下载.  


## 安装  
参见 [官方安装说明](https://docs.docker.com/engine/installation/linux/ubuntu/)  
更新仓库并安装  

1. Install packages to allow apt to use a repository over HTTPS:  
`sudo apt-get install apt-transport-https ca-certificates curl software-properties-common`  

2. Add Docker’s official GPG key:  
`curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -`  

3. Verify that the key fingerprint is 9DC8 5822 9FC7 DD38 854A E2D8 8D81 803C 0EBF CD88.  
`sudo apt-key fingerprint 0EBFCD88`

4. Use the following command to set up the stable repository. You always need the stable repository, even if you want to install edge builds as well.  
`sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"`  


5. Update the apt package index  
`sudo apt-get update`  

6. Use this command to install the latest version of Docker  
`sudo apt-get install docker-ce`  





## 卸载  
`sudo apt-get purge docker-ce`  
`sudo rm -rf /var/lib/docker`  

You must delete any edited configuration files manually.  


## 参考  
[Docker — 从入门到实践](https://www.gitbook.com/book/yeasy/docker_practice/details)  
[深入浅出Docker（一）：Docker核心技术预览](http://www.infoq.com/cn/articles/docker-core-technology-preview)  

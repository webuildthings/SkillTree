
> docker 的官方文档写的非常好,建议多去[官网](https://docs.docker.com)查询  
>  
> [命令详解 官方文档](https://docs.docker.com/engine/reference/run/)  
>  


## 速查  
`sudo docker run -it IMAGE bash`      # 启动容器  
`sudo docker start 容器名`             # 启动容器  
`sudo docker attach 容器名`            # 附着到一个运行的容器  
`sudo docker commit -m "Commit message" 容器ID  镜像名字[:TAG] `  # 将容器保存为镜像  
`sudo docker images`                  # 显示镜像  
`sudo docker ps -a`                   # 显示所有容器(运行和停止)  
`sudo docker rm 容器名`                # 删除一个容器  



## 容器  
* 启动容器 [run命令说明文档](https://docs.docker.com/engine/reference/run/)  
`docker run -it –name=指定容器名 IMAGE /bin/bash`  

* 启动容器并将本机/home目录挂载到Docker的/mnt目录下  
`docker run -it –name=指定容器名 -v /home:/mnt IMAGE bash`  


* 停止、启动、杀死一个容器  
`docker stop <容器名 or ID> `    
`docker start <容器名 or ID> `    
`docker kill <容器名 or ID> `   

* 附着到一个运行的容器上,有时需要回车才能进入   
`docker attach <容器名 or ID> `  
`docker start <ID>  && docker attach <ID> `   启动并进入容器  


* 列出一个容器里面被改变的文件或者目录，A 增加的，D 删除的，C 被改变的  
`docker diff <容器名 or ID> `  

* 重命名容器  
`docker rename oldNmae newName`  

* 显示当前运行的容器  
`sudo docker ps -a`  

* 删除所有未运行的容器  
`sudo docker rm $(sudo docker ps -a -q)`   



## 镜像  

* 保存容器为镜像  
`docker commit -m "Commit message" 容器ID  镜像名字[:TAG]`  


* 显示本地镜像列表  
`sudo docker images`  


* 利用Dockerfile创建镜像  
`docker build -f dockerFileName -t name:tag .`   

* 查看镜像历史信息  
`docker history <镜像ID>`  

* 备份和还原镜像到本地  
`docker save -o 备份名称.tar 镜像名`  
`docker load -i 备份名称.tar`  
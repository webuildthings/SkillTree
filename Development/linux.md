
# GNU Binutils (攻防)  
[https://www.gnu.org/software/binutils/](https://www.gnu.org/software/binutils/)
Binutils是一组二进制工具,提供了查询及修改目标文件的功能。  
`readelf -s xxxx.so`     readelf 命令查看ELF格式文件  
`readelf -a xxx.so | grep NEEDED`    查看动态库所链接的动态库  
`strings xxx.so`         可以查看到函数接口  
`objdump -x a.out |grep NEEDED`      查看所链接的动态库  
`ar -x xx.a`             将静态库解压出.o文件 ,ar命令用于创建及修改库   
`nm -s xx.so`           nm用于列出目标文件的符号清单  
`ldd a.out`             查看程序运行时所需的共享库  




# Valgrind 工具 [官网](http://valgrind.org/)
Valgrind是一款用于内存调试、内存泄漏检测以及性能分析的软件开发工具.  
包括如下一些工具：  
- `Memcheck`   这是应用最广泛的工具，一个重量级的内存检查器，能够发现开发中绝大多数内存错误使用情况.
- `Callgrind`  它主要用来检查程序中函数调用过程中出现的问题。
- `Cachegrind` 它主要用来检查程序中缓存使用出现的问题。
- `Helgrind`   它主要用来检查多线程程序中出现的竞争问题。
- `Massif`     它主要用来检查程序中堆栈使用中出现的问题。
- `Extension`  可以利用core提供的功能，自己编写特定的内存调试工具。

为了更好的分析，编译选项加上 `-g -O0`  不开优化等级  

## 分析堆栈  
`valgrind --tool=massif --stacks=yes ./your.exe` 运行程序，生成 massif.out 文件  
`ms_print massif.out` 使用ms_print命令解析文件  
`massif-visualizer massif.out`  使用可视化软件解析文件 

## 分析内存泄漏
`valgrind --leak-check=full ./your.exe`  


# 分析程序耗时  
在 gcc 编译标志中加上 `-pg` 
运行程序后会生成 `gmon.out` 文件  
`gprof yourExe gmon.out –b`  运行 `gprof` 分析文件   

# 解析静态库



# 解析动态库



//http://git.oschina.net/oschina/git-osc/wikis/Home
//http://backlogtool.com/git-guide/cn/contents/




//对终端显示的配置，给文字添加颜色，更易于阅读
git config --global color.diff auto  
git config --global color.status auto  
git config --global color.branch auto

//当我们向服务器提交代码时得有个用户名邮箱什么的,可以用如下命令进行设值
git config --global user.name "Your Name" 
git config --global user.email "your@email.com"

// 访问远程仓库配置
//SSH key 可以让你在你的电脑和 Git @ OSC 之间建立安全的加密连接
//生成key 并将 public key 添加到远程仓库
ssh-keygen -t rsa -C "youremail@xxx.com"




// 1. 初始化仓库 (当前目录)
git init

// 2. 添加文件
git add <file> // 添加单个文件,用空格分割可以指定多个文件
git add .      // 添加所有文件

// 3. 提交到本地仓库
git commit -m "notes"

// 4. 提交到远程仓库
git remote add origin http://git.oschina.net/whaon/GitHelloWolrd.git
git push origin master







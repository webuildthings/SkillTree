在win10下创建GIT服务器需要以下软件:     
- [Gitblit](http://gitblit.com/)    
- [JAVA](https://www.java.com/zh-CN/)    
- [Git](https://git-scm.com/download/gui/windows)    


# 一、安装java    
## 1. 下载及安装
git服务器Gitblit是基于Java一个工具，所以必须先安装java环境.      
下载地址:  [https://www.java.com/zh-CN/](https://www.java.com/zh-CN/)     
安装简单，按提示一路安装即可      
## 2. 配置环境变量
右键"计算机" --> "属性" --> "高级系统设置" --> "高级" --> "环境变量"    
### 2.1 新建"系统变量"     
变量名: `"JAVA_HOME"`      
变量值: `C:\Program Files\Java\jre1.8.0_301`  (填入自己实际安装路径)    

变量名: `CLASSPATH`    
变量值: `.;%JAVA_HOME%/lib/dt.jar;%JAVA_HOME%/lib/tools.jar`    
### 2.2 增加Path值    
修改已有变量名`Path`的值，在其最后增加两个新路径    
变量名:`Path`    
变量值:`%JAVA_HOME%\bin;%JAVA_HOME%\jre\bin`    

### 2.3 验证    
打开`cmd`命令窗口，输入 `java -version` 如果返回版本号则安装成功    
# 二、安装git
下载地址 [https://git-scm.com/downloads](https://git-scm.com/downloads)    
安装简单，根据提示安装即可    
# 三、安装Gitblit    
## 3.1 安装
下载地址: [gitblit.com](http://gitblit.com)    
下载后无需要安装，直接解压缩即可使用    
## 3.2 配置   
### 3.2.1 defaults.properties 文件    
将 `gitblit-1.9.1\data\defaults.properties` 复制一份    
并改名为`my.properties`打开找到以下配置并修改        
- `git.repositoriesFolder`     
仓库存放位置,默认在当前`git`文件夹，可以更改为自己指定的位置，如：    
`git.repositoriesFolder = E:/Git/repository`    
注意路径分隔符为左斜杠`/`，用右斜杠会出错    

- `server.httpPort`    
`server.httpPort = 10101`    
设置http协议端口,选一个未使用且不会冲突的端口即可    
- `server.httpBindInterface`    
`server.httpBindInterface = 192.168.0.197`    
配置http服务器IP地址，即本机地址

### 3.2.2 gitblit.properties 文件    
打开`gitblit-1.9.1\data\gitblit.properties`    
将`include = defaults.properties`改为`include = my.properties`    
即使用修改后的配置文件    

## 3.3 启动
执行`gitblit-1.9.1\gitblit.cmd`文件，成功后，在浏览器地址栏输入`配置的IP地址:配置的端口号`即可访问服务器,如下:    
`http://192.168.0.197:10101`    
用户名：admin，密码：admin    
登录后即可执行各种操作，新建或管理仓库     

## 3.4 开机自动启动
- 修改配置`installService.cmd`    
修改`gitblit-1.9.1\installService.cmd`文件    
找到`SET ARCH=amd64`    
在其下一行添加`SET CD=E:\Git\gitblit-1.9.1` 即设置你gitblit真实的安装路径    
找到启动参数`--StartParams`将其清空，即如下：    
`--StartParams= ""`    
保存文件    

- 添加windows服务    
以管理员权限运行刚刚修改的文件`gitblit-1.9.1\installService.cmd`    
此时可以查看`gitblit`服务已经启动,查看方法如下：        
`右键我的电脑-->管理-->服务和应用程序-->服务`
重启电脑即可正常使用        

# 四、gitblit 服务启动失败
gitblit 服务启动失败大多数情况下跟 `installService.cmd` 文件有关系    
## 4.1 ARCH没有配置对
检查`installService.cmd`中`SET ARCH`是否配置正确    
可配置为`x86`或 `amd64`    

## 4.2 检查`gitblitw.exe`程序    
使用管理员权限启动`gitblitw.exe`，检查各项配置是否与`installService.cmd`一致    
可重点检查`Java`和`Startup`两项,以下为示例，根据不同配置，值会不一样         

![1](img/gitblit-1.png) 

![2](img/gitblit-2.png)


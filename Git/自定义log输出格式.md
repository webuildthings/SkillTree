```
git config --global alias.lg "log --graph --pretty=tformat:'%Cred%h%Creset %Cgreen%ad%Creset -%C(yellow)%d%Creset %s' --abbrev-commit --date=short"
```

每个Android应用都有一个唯一的 ID , 类似`com.example.demo`  
系统通过应用ID 判断是否为同一应用        
在安装应用时，如果系统中已经存在相同的应用ID，则会覆盖安装    
所以需要在同一手机上安装多个同一项目的apk时需要修改应用ID         

要实现这个功能，可以使用以下方法:    

在`build.gradle` 中的 `buildTypes` 字段中使用 `applicationIdSuffix`      
这样就会在你的应用ID后面追加一段字符    

```
android {
    ...
    buildTypes {
        debug {
            applicationIdSuffix ".other"
        }

        release {
            applicationIdSuffix ".other"
        }
    }
}
```

这样应用ID就会变为 `com.example.demo.other`     



参考:    
官方说明  [https://developer.android.google.cn/studio/build/application-id.html#change_the_application_id_for_build_variants](https://developer.android.google.cn/studio/build/application-id.html#change_the_application_id_for_build_variants)

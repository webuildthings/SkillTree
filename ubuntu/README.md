

# 常用命令
`df -h`                显示磁盘使用情况  
`du -sh`               显示当前文件夹大小                                                                        
`find . -name 'my\*'`  搜索当前目录及子目录下所有文件名以my开头的文件  find <指定目录> <指定条件> <指定动作>  

# Ubuntu远程访问Windows桌面  
1. remmina 软件 (**推荐**)  
使用ubuntu自带软件remmina 选择 RDP 协议，远程桌面连接windows  
2. rdesktop命令  
`sudo apt-get install rdesktop`  
`rdesktop 192.168.1.100`  
`rdesktop -r shareDir:shar=/home/yourname/Downloads/ 192.168.1.100`  远程桌面，并将本地Downloads映射到远程机器(建议使用绝对路径)，名字为shareDir  

# ssh  
登录  ssh user@host  
拷贝  scp [参数] [原路径] [目标路径] 


## 公钥登录（免密码输入） 
[ssh 原理及常见问题](http://www.ruanyifeng.com/blog/2011/12/ssh_remote_login.html)  
1. ssh-keygen #生成公钥,结果输出$HOME/.ssh/目录下  
2. ssh-copy-id user@host #将公钥传输到主机上  

## 防止自动断开  
1. 客户端  
```
#打开
sudo vim /etc/ssh/ssh_config
# 添加  ,向服务器发出心跳, 每隔20秒和失败次数
ServerAliveInterval 20  
ServerAliveCountMax 999
```

2. 服务端  
```
# 打开
sudo vim /etc/ssh/sshd_config
# 添加, 服务器端向客户端发送心跳, 每隔30秒和失败次数,
ClientAliveInterval 30
ClientAliveCountMax 6
```

## 常见错误
Ubuntu apt-get update 出現 NO_PUBKEY / GPG error  

gpg --keyserver pgpkeys.mit.edu --recv-key  xxxxxxxx  
gpg -a --export xxxxxxxx | sudo apt-key add -  
apt-get update  

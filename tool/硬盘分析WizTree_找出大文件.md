
使用**WizTree**[https://wiztreefree.com/](https://wiztreefree.com/)可以在极短时间内找出硬盘里的大文件。    
在NTFS格式的硬盘上它会直接读取硬盘的MFT,所以超级快(类似Everything)    

**WizTree**    
- 免费
- 适用于Windows 10, 8, 7, Vista, XP   
- 提供安装包和绿色免安装包


下载: [https://wiztreefree.com/download](https://wiztreefree.com/download)    


![](./img/wiztree.png)
开发时我们可以使用RTT来打印log    
RTT是SEGGER公司开发的用于嵌入式程序中与用户交互的实时终端。    
RTT搭配JLink的JLinkRTTViewer软件使用    

# 1. RTT
将RTT源码移植到工程中即可实现RTT功能    
RTT源码使用ANSI C标准编写可以移植到任何嵌入式程序中    
RTT实现了一个简易的`printf()`函数`SEGGER_RTT_Printf()`,不需要堆    

RTT源码包含在JLink中,安装完JLink后就带有RTT源码[Windows JLink](https://www.segger.com/downloads/jlink/JLink_Windows.exe)    

源码在JLink安装目录下的`Samples/RTT`    
```
路径:
c/Program Files (x86)/SEGGER/JLink/Samples/
├── DCC
├── GDB
├── JFlash
├── JFlashSPI
├── JLink
├── RDI
└── RTT
    └── SEGGER_RTT_V680a.zip

内容:
SEGGER_RTT_V680a
├── Examples
│   ├── Main_RTT_InputEchoApp.c
│   ├── Main_RTT_MenuApp.c
│   ├── Main_RTT_PrintfTest.c
│   └── Main_RTT_SpeedTestApp.c
├── License.txt
├── README.txt
├── RTT
│   ├── SEGGER_RTT.c
│   ├── SEGGER_RTT.h
│   ├── SEGGER_RTT_ASM_ARMv7M.S
│   ├── SEGGER_RTT_Conf.h
│   └── SEGGER_RTT_printf.c
└── Syscalls
    ├── SEGGER_RTT_Syscalls_GCC.c
    ├── SEGGER_RTT_Syscalls_IAR.c
    ├── SEGGER_RTT_Syscalls_KEIL.c
    └── SEGGER_RTT_Syscalls_SES.c

```


RTT非常简洁，无需配置即可使用.
```
/*********************************************************************
*               SEGGER MICROCONTROLLER GmbH & Co KG                  *
*       Solutions for real time microcontroller applications         *
**********************************************************************
*                                                                    *
*       (c) 2014  SEGGER Microcontroller GmbH & Co KG                *
*                                                                    *
*       www.segger.com     Support: support@segger.com               *
*                                                                    *
**********************************************************************

----------------------------------------------------------------------
File    : RTT.c
Purpose : Simple implementation for output via RTT.
          It can be used with any IDE.
--------  END-OF-HEADER  ---------------------------------------------
*/

#include "SEGGER_RTT.h"

static void _Delay(int period) {
  int i = 100000*period;
  do { ; } while (i--);
}

int main(void) {
  do {
    SEGGER_RTT_WriteString(0, "Hello World from SEGGER!\r\n");
    _Delay(100);
  } while (1);
  return 0;
}

/*************************** End of file ****************************/
```



# 2. 使用SDK Log模块
NRF5 SDK通过nRF_Log模块实现log打印，可支持UART和RTT打印    


## 2.1 配置
主要配置    
- `NRF_LOG_ENABLED`              使能nRF_Log模块
- `NRF_LOG_BACKEND_RTT_ENABLED`  使能RTT通道
- `NRF_LOG_BACKEND_UART_ENABLED` 使能UART通道
- `NRF_LOG_DEFERRED`             立即刷新开关    
配置如下:    
![config](img/sdkLogConfig.png)
     
## 2.2 使用
- 初始化nRF_Log模块:`log_init()`     
- 打印log:`NRF_LOG_INFO()`     


# 3. 参考    
- RTT [ https://www.segger.com/jlink-rtt.html](https://www.segger.com/jlink-rtt.html)    
- RTT wiki[ https://wiki.segger.com/RTT](https://wiki.segger.com/RTT)
- 如何调试nRF5 SDK [ https://www.cnblogs.com/iini/p/9279618.html](https://www.cnblogs.com/iini/p/9279618.html)
- nRF5 Logger System[https://jimmywongiot.com/2020/02/24/print-log-flash-log-crash-log-on-nordic-nrf5-sdk/](https://jimmywongiot.com/2020/02/24/print-log-flash-log-crash-log-on-nordic-nrf5-sdk/)

`nRF Connect `是Nordic出品的通用蓝牙软件有移动版和PC版两种    
此软件功能非常强大，强烈建议安装    
下载    
[nRF Connect for Mobile](https://www.nordicsemi.com/Software-and-tools/Development-Tools/nRF-Connect-for-mobile)    
[nRF Connect for Desktop](https://www.nordicsemi.com/Software-and-tools/Development-Tools/nRF-Connect-for-desktop)

PC版本`Connect`使用简单，每个功能对应一个插件，安装对应插件即可。比如想烧录hex就安装`Programmer`插件就可以了    
**下面针对手机版本nRF Connect进行一些使用技巧说明**    

# 1. 查看本机蓝牙相关参数
nRFConnect可以查看本机蓝牙支持的特性   
![](img/nRFConnect_DeviceInformation_1.png)    
![](img/nRFConnect_DeviceInformation_2.png)    
![](img/nRFConnect_DeviceInformation_3.png)    


# 2. nRFConnect 查看自身Log
`nRFConnect App`可以查看自身收到的蓝牙数据    
使用App连接设备后，从最右上角进入`Show log`界面即可查看App收到的数据    

![log_1](img/nRFConnect_LogStep_1.png)    
![log_2](img/nRFConnect_LogStep_2.png)    
![log_3](img/nRFConnect_LogStep_3.png)    

# 3. nRFConnect 查看其它App的蓝牙通信数据
使用透传Demo进行演示
保持nRFConnect运行状态, 启动要监听的蓝牙App, `nRFToolbox`    
使用`nRFTollbox`连接设备，此时会弹出提示框，提示是否要监听    
![](img/nRFConnect_debug_1.png)    

设备向app发送 `1234567890` 和 `abcdefghijk`，查看log    
**nRFToolbox收到的数据**       
![](img/nRFConnect_debug_2.png)    
**nRFConnect监听到的数据**    
![](img/nRFConnect_debug_3.png)    

# 4. DFU
 nRFConnect支持DFU, 最右上角进入可选菜单，选择"DFU"即可进入DFU流程    
 - 进入DFU流程    
 ![](img/nRFConnect_DFU_1.png)    
 - 选择升级类型(zip)    
 ![](img/nRFConnect_DFU_2.png)    
 - 选择升级文件     
 ![](img/nRFConnect_DFU_3.png)    
 - 等待升级完成     
 ![](img/nRFConnect_DFU_4.png)    
    

nordic支持三种IDE: KEIL, IAR, SES.    
SES跟Keil非常像，是Segger公司为Nordic芯片量身打造的，对Nordic用户来说，永久免费。    
[Segger embedded studio开发环境搭建官方文档:](https://infocenter.nordicsemi.com/index.jsp?topic=%2Fug_gsg_ses%2FUG%2Fgsg%2Fintro.html&cp=1_1_0)

[Keil MDK开发环境搭建官方文档: ](https://infocenter.nordicsemi.com/index.jsp?topic=%2Fug_gsg_keil%2FUG%2Fgsg%2Fintro.html&cp=1_1_1)    

 

# 1. SES安装    
SES安装包 [https://www.segger.com/downloads/embedded-studio/](https://www.segger.com/downloads/embedded-studio/)    
# 2. nRF Command Line Tools安装
nRF5x command line tools包括Jlink驱动以及Nordic自己开发的一些命令行工具    
具体包括Jlink驱动，nrfjprog，nrfutil以及mergehex等。    
下载链接为：[https://www.nordicsemi.com/Software-and-Tools/Development-Tools/nRF-Command-Line-Tools/Download#infotabs](https://www.nordicsemi.com/Software-and-Tools/Development-Tools/nRF-Command-Line-Tools/Download#infotabs)
# 3. SDK安装    
sdk包含各种例程及协议栈hex, SDK是个压缩包，解压即可用。    
# 4. SDK插件安装    
## 4.1 SES无需安装插件, KEIL, IAR需要安装。    
SDK有个配置文件sdk_config.h是按照CMSIS规范写的，Keil可以通过图形界面去更改sdk_config.h里面的内容。SES需要添加脚本才能使用图像界面配置。    


打开SES，选择 File -> Open Studio Folder... -> External Tools Configuration，然后tools.xml文件将会打开，在 </tools>行之前插入如下文本 :    
```
<item name="Tool.CMSIS_Config_Wizard" wait="no">

    <menu>&amp;CMSIS Configuration Wizard</menu>

    <text>CMSIS Configuration Wizard</text>

    <tip>Open a configuration file in CMSIS Configuration Wizard</tip>

    <key>Ctrl+Y</key>

    <match>*config*.h</match>

    <message>CMSIS Config</message>

    <commands>

      java -jar &quot;$(CMSIS_CONFIG_TOOL)&quot; &quot;$(InputPath)&quot;

    </commands>

</item>
```
 注意：由于CMSIS Configuration Wizard是一个Java应用程序，所以你必须先安装Java运行时环境（JRE）才能运行该工具。


## 4.2 keil5  
### 4.2.1 ARM CMSIS安装
编译例程时会提示缺少CMSIS, 缺少哪个就安装哪个. 下载地址[https://github.com/ARM-software/CMSIS/releases]https://github.com/ARM-software/CMSIS/releases    
### 4.2.2 Device family pack（又称nRF MDK）安装
nRF MDK下载链接为:[https://www.nordicsemi.com/Software-and-Tools/Development-Tools/nRF-MDK/Download#infotabs](https://www.nordicsemi.com/Software-and-Tools/Development-Tools/nRF-MDK/Download#infotabs)    
选择你的IDE和平台（Keil5选择pack哦，BSD license和Nordic license二选其一即可，SDK17推荐使用BSD license，SDK16推荐使用Nordic license）以及相应的版本    
编译SDK例程时提示选择哪个版本就安装哪个版本    
*重要提示：请不要让Keil自动下载Device family pack，请按照上面的方法，手动去官网下载和安装，否则极有可能出现编译失败的情况*
## 4.3 keil4   
SDK自带插件安装包，在SDK根目录下： nRF5x_MDK_8_16_0_Keil4_NordicLicense.msi
## 4.4 IAR
SDK自带插件安装包，在SDK根目录下：nRF5x_MDK_8_16_0_IAR_NordicLicense.msi    
SDK IAR例子默认都使用IAR7进行测试和开发的，由于IAR8和IAR7有一点点不兼容，使用IAR8直接编译某些带库的例子会报错，请参考SDK目录：nRF5_SDK_15.x\documentation里面的release_notes.txt中提供的解决方案来解决IAR8编译报错问题
# 5.nRF connect安装
nRF connect有桌面版本及手机版本都建议安装。    
手机app: [https://github.com/NordicSemiconductor/Android-nRF-Connect/releases](https://github.com/NordicSemiconductor/Android-nRF-Connect/releases) . 官网也可以下载    
桌面版本:[https://www.nordicsemi.com/Software-and-Tools/Development-Tools/nRF-Connect-for-desktop/Download#infotabs](https://www.nordicsemi.com/Software-and-Tools/Development-Tools/nRF-Connect-for-desktop/Download#infotabs)



# 参考:    
[Nordic nRF51/nRF52开发环境搭建](https://www.cnblogs.com/iini/p/9043565.html)
[Nordic tools and downloads](https://infocenter.nordicsemi.com/index.jsp?topic=%2Fug_getting_started%2FUG%2Fgs%2Fproduct_development.html&cp=1_0)





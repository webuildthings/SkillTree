nRF52 系列有些IO口有多个功能可选，如果不配置则使用默认功能。具体可查看芯片手册。    
NFC对应的IO口默认为NFC功能,如果要使用普通IO功能则必须手动配置为IO口。    
nRF52832 NFC 对应的IO口为     
`NFC1-->P0.09`     
`NFC2-->P0.10`    
要用这两个管脚做普通IO则必须做配置：    
在IDE中预定义宏``CONFIG_NFCT_PINS_AS_GPIOS``则将NFC对应的IO口设置为普通IO口.    
![NFC](img/NFC_PreprocessorDefinitons.png)



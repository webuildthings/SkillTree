nrf52832有三个 RTC    
协议栈使用了RTC0, APP_TIMER使用了RTC1    
我们可以利RTC2实现日历    


1. RTC 配置    
rtc频率如下：
```
f [kHz] = 32.768 / (PRESCALER + 1 )
```
2. 休眠
noridc有两种休眠模式 System OFF 和 System ON    
System OFF 模式是深度休眠模式，将关闭cpu和所有外设    
System ON  模式可以关闭cpu后外设继续工作    
当使用RTC功能时只能使用System ON模式休眠，否则RTC会停止工作    

3. 实现
参见官方demo [https://github.com/NordicPlayground/nrf5-calendar-example](https://github.com/NordicPlayground/nrf5-calendar-example)
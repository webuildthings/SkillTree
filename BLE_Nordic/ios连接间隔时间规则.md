iOS对蓝牙连接参数有明确要求。之前项目中的连接参数不符合要求，通信过程中会自动断开。    
最新参数要求请查看 :     
[https://developer.apple.com/accessories/Accessory-Design-Guidelines.pdf](https://developer.apple.com/accessories/Accessory-Design-Guidelines.pdf)         
    

```
35.6 Connection Parameters
The connection parameter request may be rejected if it does not comply with all of these rules:
- Bluetooth device latency 30
- 2 seconds connSupervisionTimeout 6 seconds
- Interval Min modulo 15 ms == 0
- Interval Min ≥15 ms
- One of the following:
  - Interval Min + 15 ms Interval Max
  - Interval Min == Interval Max == 15 ms
- Interval Max * (Bluetooth device latency + 1) 2 seconds
- Interval Max * (Bluetooth device latency + 1) * 3 < connSupervisionTimeout
Note that if an accessory requests Interval Min == Interval Max == 15 ms, some devices will scale the interval to 30 ms to balance power and performance constraints.
```

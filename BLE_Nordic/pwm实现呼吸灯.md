Nordic的引脚是可以自由定义的。只有SAADC接口是固定的那几个引脚，    
数字引脚，PWM, I2C, UART, I2S都是可以自由定义的。    

# 1. 使用能PWM
配置`sdk_config.h`，使能PWM    
```
#define NRFX_PWM_ENABLED 1
#define PWM_ENABLED 1
#define PWM0_ENABLED 1
#define NRFX_PWM0_ENABLED 0
```

# 2. 实例化
总共支持4个PWM，分别是PWM0, PWM1, PWM2, PWM3 .    
使用PWM0    
`static nrf_drv_pwm_t m_pwm0 = NRF_DRV_PWM_INSTANCE(0);`


# 3. 初始化
```
static void pwmInit(void)
{    

    nrf_drv_pwm_config_t const config0 =
    {
        .output_pins =
        {
            10 | NRF_DRV_PWM_PIN_INVERTED, // channel 0
            NRF_DRV_PWM_PIN_NOT_USED, // channel 1
            NRF_DRV_PWM_PIN_NOT_USED, // channel 2
            NRF_DRV_PWM_PIN_NOT_USED  // channel 3
        },
        .irq_priority = APP_IRQ_PRIORITY_LOWEST,
        .base_clock   = NRF_PWM_CLK_1MHz,
        .count_mode   = NRF_PWM_MODE_UP,
        .top_value    = 10000,
        .load_mode    = NRF_PWM_LOAD_INDIVIDUAL,
        .step_mode    = NRF_PWM_STEP_AUTO
    };
    APP_ERROR_CHECK(nrf_drv_pwm_init(&m_pwm0, &config0, pwm_red_handler));    

}


```

# 4. 回调函数,呼吸效果实现

```
static int gTmpRedCnt = 0;
static void pwm_red_handler(nrf_drv_pwm_evt_type_t event_type)
{
    if (event_type == NRF_DRV_PWM_EVT_FINISHED)
    {

        uint16_t * p_channels = (uint16_t *)&(gRedSeqValues.channel_0);


        // 呼吸效果(-5000, 5000对应 top_value=10000)
        *p_channels = (-5000) * cos(3.14/1500 * (gTmpRedCnt)) + 5000;  
        gTmpRedCnt += 10;

        if(gTmpRedCnt >= 0xFFFFFF00){
            gTmpRedCnt = 0;        
        }        

    }
}
```


# 5. 运行
```
static nrf_pwm_values_individual_t gRedSeqValues={0};
static nrf_pwm_sequence_t const    gRedSeq =
{
    .values.p_individual = &gRedSeqValues,
    .length              = NRF_PWM_VALUES_LENGTH(gRedSeqValues),
    .repeats             = 0,
    .end_delay           = 0
};
nrf_drv_pwm_simple_playback(&m_pwm0, &gRedSeq, 1,NRF_DRV_PWM_FLAG_LOOP);
```

# 参考
- [蓝牙芯片nRF52832之PWM的使用](https://www.jianshu.com/p/76f0cd558710)
- [nRF52 PWM 使用](https://blog.csdn.net/leirifa/article/details/86582593)
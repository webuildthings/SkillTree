DFU : Device Firmware Update 即固件升级     

DFU可分为两种:        
`dual bank`   : 先接收后升级.   优点：升级失败可回滚  缺点:需要额外空间        
`single bank` : 边接收边升级.    优点: 节约空间   缺点:如果出错不能回滚版本    

最新SDK(v15.3.0)会根据当前代码空间,自动选择使用`dual bank`或`single bank`    
Nordic 支持三种升级方式: UART, USB, BLE    

Nordic要实现DFU需要两个独立的代码:`bootloader` 和 `app`     
下面为使用流程:     

# 1. Bootloader    
Nordic有两种bootloader: 开放式和安全式    
开放式不做安全校验     
安全式要做安全校验，只有校验通过的才生效    

## 1.1 生成micro-ecc算法库    
加密bootloader会使用到`micro-ecc`算法，这需要我们手动编译并生成`micro-ecc`算法库    
确保本机装有git和GCC    
进入文件夹`SDK根目录\external\micro-ecc`运行`build_all`脚本即可自动生成`micro-ecc`算法库

## 1.2 生成公钥及私钥
例程位置: `SDK根目录\examples\dfu\secure_bootloader`    
安全式bootloader需要Key    
私钥生成命令：`nrfutil keys generate priv.pem`     
公钥生成命令：`nrfutil keys display --key pk --format code priv.pem --out_file dfu_public_key.c`     

`priv.pem` 就是私钥, 名字可改,生成升级包时需要使用此私钥，必需妥善保管，否则将永远不能升级    
`dfu_public_key.c` 就是公钥

## 1.3 生成bootloader hex
将生成的公钥`dfu_public_key.c`覆盖`SDK根目录\examples\dfu`下的同名文件,并生成`bootloader.hex`     

# 2. App
生成自己的应用程序，假如取名为: `app.hex`     


# 3. settings

生成setting    
```
nrfutil settings generate --family NRF52 --application app.hex --application-version 1 --bootloader-version 1 --bl-settings-version 1 settings.hex
```

# 4. 生成最终烧录Hex
合并hex: setting, bootloader, softdevice, app    
```
mergehex --merge bootloader.hex settings.hex --output bl_temp.hex 
mergehex --merge bl_temp.hex app.hex s132_nrf52_7.0.1_softdevice.hex --output whole.hex
```

# 5. 生成升级zip包

```
nrfutil pkg generate --application app_new.hex --application-version 2 --hw-version 52 --sd-req 0xCB --key-file priv.pem APP_DFU_PACKET.zip
```
`--application`表示新固件hex文件    
`--key-file` 表示签名用的私钥文件    
`--sd-req`表示协议栈softdevice版本 可以通过命令`nrfutil pkg generate --help`获得列表    


# 6. 使用nRFConnect DFU
nRFConnect支持DFU功能，直接选择待升级的升级zip包`APP_DFU_PACKET.zip`即可完成升级.     
具体流程见  [强大的辅助软件nRFConnect](强大的辅助软件nRFConnect.md)        


# 7. 手机端DFU
官方提供Android 及 iOS平台的DFU库:     
- Android版DFU库：[https://github.com/NordicSemiconductor/Android-DFU-Library](https://github.com/NordicSemiconductor/Android-DFU-Library)     
- iOS版DFU库：[https://github.com/NordicSemiconductor/IOS-Pods-DFU-Library](https://github.com/NordicSemiconductor/IOS-Pods-DFU-Library)     

官方还提供一个开源app  `nRF Toolbox`,内集成多个功能，包含DFU:    
- Android版本 [https://github.com/NordicSemiconductor/Android-nRF-Toolbox](https://github.com/NordicSemiconductor/Android-nRF-Toolbox)    
- iOS版本: [https://github.com/NordicSemiconductor/IOS-nRF-Toolbox](https://github.com/NordicSemiconductor/IOS-nRF-Toolbox)     


# 参考
- [详解蓝牙空中升级(BLE OTA)原理与步骤](https://www.cnblogs.com/iini/p/9314246.html) (非常详细，推荐)    
- [Bootloader and DFU modules](https://infocenter.nordicsemi.com/index.jsp?topic=%2Fcom.nordic.infocenter.sdk5.v15.3.0%2Flib_bootloader_dfu_banks.html&cp=7_5_0_3_5_1_2)
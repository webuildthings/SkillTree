现象:     
nRF Connect PC版打开后提示错误，导致无法添加或更新app    
![](img/nRFConnect_PC_Error.png)    
根据提示，手动访问 [https://raw.githubusercontent.com/NordicSemiconductor/pc-nrfconnect-core/master/apps.json.](https://raw.githubusercontent.com/NordicSemiconductor/pc-nrfconnect-core/master/apps.json.)    
提示不能访问，看来是网络访问问题

解决方法:     
手动更新`C:\Windows\System32\drivers\etc\hosts`文件，添加IP地址    
使用[https://www.ipaddress.com/](https://www.ipaddress.com/) 获取到 [https://raw.githubusercontent.com](https://raw.githubusercontent.com)的IP地址为 `199.232.68.133`    
将下面信息添加到`hosts`中，刷新DNS后能正常访问    
```
199.232.68.133	  raw.githubusercontent.com   
```

  


# 参考
[https://devzone.nordicsemi.com/f/nordic-q-a/61509/how-to-add-app-sources-on-nrf-connect-v3-3-1](https://devzone.nordicsemi.com/f/nordic-q-a/61509/how-to-add-app-sources-on-nrf-connect-v3-3-1)

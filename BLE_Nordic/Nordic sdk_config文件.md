Nordic BLE开发工程中，`sdk_config.h` 文件非常重要，    
当基于SDK例程增加功能时会需要对`sdk_config.h`进行修改，添加对应的宏进行配置    
这时可以从`sdk_config.h`模板文件中查看对应功能模块所需要的宏    
路径为: `SDK\config`    
```
SDK/
└── config
    ├── nrf52810
    │   ├── armgcc
    │   ├── config
    │   │   └── sdk_config.h
    │   └── ses
    ├── nrf52811
    │   ├── armgcc
    │   ├── config
    │   │   └── sdk_config.h
    │   └── ses
    ├── nrf52832
    │   ├── armgcc
    │   ├── config
    │   │   └── sdk_config.h
    │   └── ses
    └── nrf52840
        ├── armgcc
        ├── config
        │   └── sdk_config.h
        └── ses

```
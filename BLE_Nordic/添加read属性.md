# 关键点
Nordic Ble服务添加读功能，有两个关键点：    
1. 设置read属性    
2. 在服务事件中添加读事件处理     

## 1.使能read属性
```
ble_add_char_params_t add_char_params;
memset(&add_char_params, 0, sizeof(add_char_params));
add_char_params.char_props.read = 1;
add_char_params.read_access     = SEC_OPEN;
```

## 2. 在服务事件中添加读事件处理，上报数据    
```
void ble_infor_on_ble_evt(ble_evt_t const * p_ble_evt, void * p_context)
{
    ble_infor_t * p_infor = (ble_infor_t *)p_context;
    switch (p_ble_evt->header.evt_id)
    {
        case BLE_GATTS_EVT_RW_AUTHORIZE_REQUEST:
            //处理GATT读写权限请求事件
            authorize_request(p_ble_evt, p_infor);
            break;
    }
}

```



# 实例    
以`SDK\components\ble\ble_services\ble_lbs`为模板修改为带读写MTU值的infor服务    
注:这里说的mtu值是指业务逻辑层做的封装层的值，而不是真正硬件配置的MTU值。    
`请将以下的mtu值理解为一个全局变量`

## ble_info.h
```
#ifndef BLE_INFOR_H__
#define BLE_INFOR_H__

#include <stdint.h>
#include <stdbool.h>
#include "ble.h"
#include "ble_srv_common.h"
#include "nrf_sdh_ble.h"

#ifdef __cplusplus
extern "C" {
#endif

/**@brief   Macro for defining a ble_infor instance.
 * @param   _name   Name of the instance.
 * @hideinitializer
 */

#define BLE_INFOR_BLE_OBSERVER_PRIO  2

#define BLE_INFOR_DEF(_name)                                                                          \
static ble_infor_t _name;                                                                             \
NRF_SDH_BLE_OBSERVER(_name ## _obs,                                                                 \
                     BLE_INFOR_BLE_OBSERVER_PRIO,                                                     \
                     ble_infor_on_ble_evt, &_name)


// Forward declaration of the ble_lbs_t type.
typedef struct ble_infor_s ble_infor_t;

typedef void (*ble_mtu_write_handler_t) (uint16_t conn_handle, ble_infor_t * p_info, uint8_t mtu_value);
typedef void (*ble_mtu_read_handler_t) (uint16_t conn_handle, ble_infor_t * p_info, uint8_t *mtu_value);

/** @brief LED Button Service init structure. This structure contains all options and data needed for
 *        initialization of the service.*/
typedef struct
{
    uint8_t *pVersion;              /**version point */
    ble_mtu_write_handler_t   mtu_write_handler;  /**set mtu*/
    ble_mtu_read_handler_t    mtu_read_handler;   /**get mtu*/
} ble_infor_init_t;

/**@brief LED Button Service structure. This structure contains various status information for the service. */
struct ble_infor_s
{
    uint8_t                     uuid_type;           /**< UUID type for the LED Button Service. */
    uint16_t                    service_handle;      /**< Handle of INFORMATION Service (as provided by the BLE stack). */    
    ble_gatts_char_handles_t    mtu_char_handles;    /**< Handles related to the MTU Characteristic. */
    
    ble_mtu_write_handler_t     mtu_write_handler;   /**< Event handler to be called when the MTU Characteristic is written. */
    ble_mtu_read_handler_t     mtu_read_handler;   /**< Event handler to be called when the MTU Characteristic is written. */    
};


extern uint32_t ble_infor_init(ble_infor_t * p_infor, const ble_infor_init_t * p_infor_init);

extern void ble_infor_on_ble_evt(ble_evt_t const * p_ble_evt, void * p_context);


#ifdef __cplusplus
}
#endif

#endif // BLE_INFOR_H__
```

## ble_info.c    
```
#include "sdk_common.h"
#include "nrf_log.h"
#include "app_error.h"
#include "ble_infor.h"

/**< Used vendor specific UUID. */
#define INFOR_UUID_BASE       {0x8E, 0xCC, 0xDD, 0x24, 0x0E, 0xE5, 0xA9, 0xE0, 0x93, 0xF3, 0xA3, 0xB5, 0x00, 0x00, 0x40, 0x8E} 
#define INFOR_UUID_SERVICE              0x0001
#define INFOR_UUID_MTU_CHAR             0x0003
#define BLE_INFOR_MAX_MTU_CHAR_LEN      1       //1个字节(characteristic 值的长度,即上报数据的长度)

uint32_t ble_infor_init(ble_infor_t * p_infor, const ble_infor_init_t * p_infor_init)
{
    uint32_t              err_code;
    ble_uuid_t            ble_uuid;
    ble_add_char_params_t add_char_params;

    // Initialize service structure.
    p_infor->pVersion = p_infor_init->pVersion;
    p_infor->mtu_write_handler = p_infor_init->mtu_write_handler;
    p_infor->mtu_read_handler = p_infor_init->mtu_read_handler;

    /**@snippet [Adding proprietary Service to the SoftDevice] */
    // Add a custom base UUID.
    ble_uuid128_t base_uuid = {INFOR_UUID_BASE};
    err_code = sd_ble_uuid_vs_add(&base_uuid, &p_infor->uuid_type);
    VERIFY_SUCCESS(err_code);

    ble_uuid.type = p_infor->uuid_type;
    ble_uuid.uuid = INFOR_UUID_SERVICE;

    // Add the service.
    err_code = sd_ble_gatts_service_add(BLE_GATTS_SRVC_TYPE_PRIMARY,
                                        &ble_uuid,
                                        &p_infor->service_handle);
    /**@snippet [Adding proprietary Service to the SoftDevice] */
    VERIFY_SUCCESS(err_code);

    // Add the MTU Characteristic.
    memset(&add_char_params, 0, sizeof(add_char_params));
    add_char_params.uuid                     = INFOR_UUID_MTU_CHAR;
    add_char_params.uuid_type                = p_infor->uuid_type;
    add_char_params.max_len                  = BLE_INFOR_MAX_MTU_CHAR_LEN;
    add_char_params.init_len                 = sizeof(uint8_t);
    add_char_params.is_var_len               = true;
    add_char_params.char_props.write         = 1;
    //add_char_params.char_props.write_wo_resp = 1;
    add_char_params.char_props.read          = 1;

    add_char_params.read_access  = SEC_OPEN;
    add_char_params.write_access = SEC_OPEN;
    add_char_params.is_defered_read = 1;    

    err_code = characteristic_add(p_infor->service_handle, &add_char_params, &p_infor->mtu_char_handles);
    if (err_code != NRF_SUCCESS)
    {
        return err_code;
    }
}


/**@brief Function for handling the Write event.
 *
 * @param[in] p_lbs      LED Button Service structure.
 * @param[in] p_ble_evt  Event received from the BLE stack.
 */
static void on_write(ble_infor_t * p_infor, ble_evt_t const * p_ble_evt)
{
    ble_gatts_evt_write_t const * p_evt_write = &p_ble_evt->evt.gatts_evt.params.write;
    if (   (p_evt_write->handle == p_infor->mtu_char_handles.value_handle)
        && (p_evt_write->len == 1)
        && (p_infor->mtu_write_handler != NULL))
    {
        p_infor->mtu_write_handler(p_ble_evt->evt.gap_evt.conn_handle, p_infor, p_evt_write->data[0]);
    }
}


void authorize_request(ble_evt_t const * p_ble_evt, ble_infor_t * p_infor){
    uint32_t  err_code;
    int tmp = 0;
    uint8_t tmpValueMTU=100;
    uint16_t uuid=0;;

    /* reply with SUCCESS/ALLOWED */
    ble_gatts_rw_authorize_reply_params_t rw_authorize_reply_params;

    if(p_ble_evt->evt.gatts_evt.params.authorize_request.type == BLE_GATTS_AUTHORIZE_TYPE_READ){
        
        memset(&rw_authorize_reply_params, 0, sizeof(rw_authorize_reply_params));
        rw_authorize_reply_params.type = BLE_GATTS_AUTHORIZE_TYPE_READ;
        rw_authorize_reply_params.params.read.gatt_status = BLE_GATT_STATUS_SUCCESS;
        rw_authorize_reply_params.params.read.update = 1;
        uuid = p_ble_evt->evt.gatts_evt.params.authorize_request.request.read.uuid.uuid;
        if(uuid== INFOR_UUID_MTU_CHAR){
            // 准备数据
            p_infor->mtu_read_handler(p_ble_evt->evt.gap_evt.conn_handle, p_infor,&tmpValueMTU);
            rw_authorize_reply_params.params.read.p_data=&tmpValueMTU;
            
            //当前长度固定为最大值，否则需要判断并处理长度是否越界 (tmp > BLE_INFOR_MAX_MTU_CHAR_LEN)?(BLE_INFOR_MAX_MTU_CHAR_LEN):(tmp);
            rw_authorize_reply_params.params.read.len = BLE_INFOR_MAX_MTU_CHAR_LEN;
            
            // 发送数据
            err_code = sd_ble_gatts_rw_authorize_reply(p_ble_evt->evt.gatts_evt.conn_handle, &rw_authorize_reply_params);
            APP_ERROR_CHECK(err_code);        
        }
    }
}

// 事件处理
void ble_infor_on_ble_evt(ble_evt_t const * p_ble_evt, void * p_context)
{
    ble_infor_t * p_infor = (ble_infor_t *)p_context;

    switch (p_ble_evt->header.evt_id)
    {
        case BLE_GATTS_EVT_WRITE:
            // 写事件
            on_write(p_infor, p_ble_evt);
            break;

        case BLE_GATTS_EVT_RW_AUTHORIZE_REQUEST:
            // 读写授权事件(读事件在此处理)
            authorize_request(p_ble_evt, p_infor);
            break;

        default:
            // No implementation needed.
            break;
    }
}
```
## main.c
```
BLE_INFOR_DEF(m_infor);  // 定义实例 并 注册ble_infor_on_ble_evt()事件回调函数

static uint8_t gMTU = 247;

// mtu 设置
void mtuWriteHandle (uint16_t conn_handle, ble_infor_t * p_info, uint8_t mtu_value){
  gMTU = mtu_value;
}

// mtu 获取
void mtuReadHandle(uint16_t conn_handle, ble_infor_t * p_info, uint8_t *mtu_value){
  *mtu_value = gMTU;
}

//添加服务
uint32_t services_init(void){
    uint32_t    err_code;    

    ble_infor_init_t inforInit;
    inforInit.mtu_write_handler = mtuWriteHandle;
    inforInit.mtu_read_handler  = mtuReadHandle;

    err_code = ble_infor_init(&m_infor, &inforInit);

    return err_code;

}


int main(void){
    ...
    services_init();
    ...
}
```

